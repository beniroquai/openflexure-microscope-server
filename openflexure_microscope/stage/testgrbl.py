import serial
import time

ser = serial.Serial("/dev/ttyUSB0")
ser.write("\r\n\r\n".encode())# Wake up grbl
time.sleep(1)   # Wait for grbl to initialize 
ser.flushInput()  # Flush startup text in serial input

ser.write(("?" + '\n').encode())
return_message = ''
#while not b'ok\r\n'==self.serial_xyz.readline():
grbl_out = ser.readline() # Wait for grbl response with carriage return
return_message = (ser.strip()).decode()

