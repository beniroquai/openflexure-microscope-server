# HARDWARE 

This folder should give you some additional tools which you may want to integrate into the openflexure ecosystem or alterantively use as a standalone (e.g. serial) device. 

So far there is a driver which connects a voice coil motor driven Lens (Optical Pickup Unit) and a Laser using an ESP32 as a serial device. 

## Setup

Please flash the `.ino` file on your ESP32. It's located in [ESP32/ESP32_lenslaser/ESP32_lenslaser.ino](ESP32/ESP32_lenslaser/ESP32_lenslaser.ino). 

The wiring follows the cellSTORM stuff which (for now) can be found [here](https://github.com/beniroquai/dSTORM-on-a-Chi-ea-p/tree/master/ELECTRONICS).

In order to test the serial device copy this folder to your local machine and open a Python command interface. There is a [Juypter Notebook](Notebook_testlens.ipynb) which you can use to test the device. 

## Prerequirements

```
pyserial
numpy
jupyter notebook
```

## Trouble shoot

To detect a serial device on linux you can try typing this:

```
dmesg | tail
```

it may give you something like ```ttyUSB0``` which needs to be filled in the serial device variable. 

Good luck! :-) 

