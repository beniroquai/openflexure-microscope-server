from labthings.extensions import BaseExtension
from labthings import find_component, fields
from labthings.views import ActionView
from typing import Tuple
from labthings.schema import Schema

import time
import io  # Used in our capture action
import logging
import numpy as np

# Used to run our stagecalib in a background thread
from labthings import update_action_progress as update_task_progress

from openflexure_microscope.captures.capture_manager import (
    generate_basename,
)

# Used to convert our GUI dictionary into a complete eV extension GUI
from openflexure_microscope.api.utilities.gui import build_gui


## Extension methods
def move_stage(
    microscope,
    task_name,
    metadata: dict = {}
):

    with microscope.stage.lock:

        if task_name == 'Homing':
            microscope.stage.go_home()
        elif task_name == 'Focus Calibration':
            # first we want to move the stage to the end position
            microscope.stage.go_home()

            posx_max = 36000
            posy_max = 55000
            myposarray = np.array(((0,0,0), (posx_max,0,0),(posx_max,posy_max,0),(0,posy_max,0)))

            for _ in range(2):
                for i in range(myposarray.shape[0]):
                    print("Move to: " +str(myposarray[i,:])+"mm")
                    microscope.stage.move_abs((myposarray[i,0],myposarray[i,1],myposarray[i,2]))
                    print("check focus")
                    time.sleep(10)
            print("Done with stage calibration..")



## Extension views
class StageCalibAPI(ActionView):
    """
    Record a stagecalib
    """
    args = {
        "task_name": fields.String(
            missing="Homing", example="Homing", description="Task to be done by the stage"
        )
    }

    def post(self, args):
        task_name = args.get("task_name")

        # Find our microscope component
        microscope = find_component("org.openflexure.microscope")

        return move_stage(
            microscope,
            task_name,
            metadata=microscope.metadata,
        )


## Extension GUI (OpenFlexure eV)
# Alternate form without any dynamic parts
extension_gui = {
    "icon": "query_builder",  # Name of an icon from https://material.io/resources/icons/
    "forms": [  # List of forms. Each form is a collapsible accordion panel
        {
            "name": "Acquire a stagecalib series",  # Form title
            "route": "/stagecalib",  # The URL rule (as given by "add_view") of your submission view
            "isTask": True,  # This forms submission starts a background task
            "isCollapsible": False,  # This form cannot be collapsed into an accordion
            "submitLabel": "Start Process",  # Label for the form submit button
            "schema": [  # List of dictionaries. Each element is a form component.
                {
                    "fieldType": "selectList",
                    "name": "task_name",
                    "label": "Task",
                    "value": "Focus Calibration",
                    "options": ["Focus Calibration","Homing"],
                }
            ],
        }
    ],
}


## Create extension

# Create your extension object
stagecalib_extension = BaseExtension("org.openflexure.stagecalib_extension", version="0.0.0")

# Add methods to your extension
stagecalib_extension.add_method(move_stage, "move_stage")

# Add API views to your extension
stagecalib_extension.add_view(StageCalibAPI, "/stagecalib")

# Add OpenFlexure eV GUI to your extension
stagecalib_extension.add_meta("gui", build_gui(extension_gui, stagecalib_extension))
