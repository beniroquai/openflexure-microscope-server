from .LedGrid import LedGrid
import board
#from adafruit_dotstar import DotStar
import numpy as np

class DotStar():
    def __init__(self, board, mosi, size):
        self.leds = []
        for _ in range(size): 
            self.leds.append((0,0,0))
        return None #self.leds
        

class DotStarGrid(LedGrid):
    def write_to_led(self):
        offset = np.array(self.offset)
        offset_y_start = offset[0,0]
        offset_x_start = offset[0,1]

        array_width = self.width + offset[:,1].sum()
        array_height = self.height + offset[:,0].sum()
        pixels = DotStar(board.SCK, board.MOSI, array_width*array_height)
        if(0):
            for i in range(array_width*array_height):
                print( (0,0,0)) #pixels[i] = (0,0,0)

            for y in range(self.height):
                for x in range(self.width):
                    print( self.state_matrix[y,x]) # pixels[(y+offset_y_start)*array_width + x + offset_x_start] = self.state_matrix[y,x]
        #pixels.show()