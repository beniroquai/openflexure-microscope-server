from labthings import find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.utilities import path_relative_to
from labthings.find import registered_extensions
from openflexure_microscope.api.utilities.gui import build_gui
from .ScriptExtensionViews import RunScriptView, SyntaxHelpView
from time import time, sleep
import numpy as np
import re
import logging

class Command:
    def __init__(self, extension, name, definition):
        self.argument_definitions = []
        self.keyword_argument_definitions = {}
        self.extension = extension
        self.name = name
        for arg in definition["args"]:
            self.argument_definitions.append(self._parse_definition_string(arg))

        for key, arg in definition["kwargs"].items():
            self.keyword_argument_definitions[key] = self._parse_definition_string(arg)

        self.extension_function = definition["function"]
        self.required_locks = definition["locks"]
        self.description = definition["description"]

    def run(self, arguments = [], keyword_arguments = {}):
        if not self.validate(arguments, keyword_arguments):
            return False
        if self.extension_function(self.extension, *arguments, **keyword_arguments) is False:
            return False

        return True

    def validate(self, arguments, keyword_arguments):
        """
            Check if set of arguments and keyword arguments is a valid call
        """
        if len(arguments) > len(self.argument_definitions):
            return False

        for i, argument_definition in enumerate(self.argument_definitions):
            if len(arguments) < i and not argument_definition["optional"]:
                return False

            if len(arguments) > i and not self._check_argument(argument_definition, arguments[i]):
                return False
        
        for key, argument_definition in self.keyword_argument_definitions.items():
            if not argument_definition["optional"] and not key in keyword_arguments:
                return False

            if key in keyword_arguments and not self._check_argument(argument_definition, keyword_arguments[key]):
                return False

        for key in keyword_arguments:
            if key not in self.keyword_argument_definitions:
                return False

        return True
            

    def _check_argument(self, argument_definition, argument):
        if argument_definition["type"] == "int":
            try:
                argument = int(argument)
            except ValueError:
                return False
        elif argument_definition["type"] == "float":
            try:
                argument = float(argument)
            except ValueError:
                return False
        elif argument_definition["type"] == "str" or argument_definition["type"] == "any":
            return True
        return True


    def _parse_definition_string(self, definitionString):
        """
        Parse definition string format to a standard dict for processing

        :param definitionString: string to be parsed
        :type definitionString: str
        """
        match = re.match("^\??([a-zA-Z]+):", definitionString)
        if match:
            argType = match.group(1)
        else:
            argType = "any"

        split = definitionString.split(":", 1)
        if len(split) == 2:
            description = split[1]
        else:
            description = definitionString.strip("?")

        return {
            "optional": definitionString.startswith("?"),
            "type": argType,
            "description": description 
        }
            

# Create the extension class
class ScriptExtension(BaseExtension):
    _commands = {}

    def __init__(self):
        super().__init__("org.openflexure.scripting", version="0.0.1", static_folder = path_relative_to(__file__, "static"))
        
        def gui_func():
            return {
                "icon": "code",
                "frame": {
                    "href": self.static_file_url("")
                }
            }

        self.add_view(RunScriptView, "/run")
        self.add_view(SyntaxHelpView, "/help")
        self.add_meta("gui", build_gui(gui_func, self))

    def get_commands(self):
        if len(self._commands.items()) > 0:
            return self._commands

        for name, command_definition in self.script_commands.items():
            self._commands[name] = Command(self, name, command_definition)

        for extension in registered_extensions().values():
            if not hasattr(extension, "script_commands"):
                continue
            for name, command_definition in extension.script_commands.items():
                self._commands[name] = Command(extension, name, command_definition)

        return self._commands

    def move(self, x, y, z):
        microscope = find_component("org.openflexure.microscope")
        if microscope.stage is None:
            return False

        return microscope.stage.move_rel([x,y,z])

    def image(self, filename = None, **kwargs):
        microscope = find_component("org.openflexure.microscope")
        return microscope.capture(
                filename=filename,
                temporary="temporary" in kwargs and kwargs["temporary"] == 1,
                use_video_port="video_port" in kwargs and kwargs["video_port"] == 1,
                #resize=resize,
                #bayer=args.get("bayer"),
                #annotations=args.get("annotations"),
                #tags=args.get("tags"),
            )

    def delay(self, time):
        sleep(float(time))
        return True

    script_commands = {
        "move": {
            "function": move,
            "description": "Move stage", 
            "args": [
                "int:X-steps",
                "int:Y-steps",
                "int:Z-steps"
            ],
            "kwargs": {},
            "locks": ["stage"]
        },
        "image": {
            "function": image,
            "description": "Save image",
            "args": [
                "?str:Filename",
            ],
            "kwargs": {
                "temporary": "?int:Is temporary (0,1)",
                "video_port": "?int:Use video port (0,1)"
            },
            "locks": ["camera"]
        },
        "delay": {
            "function": delay,
            "description": "Sleep",
            "args": [
                "float:Sleep time in seconds"
            ],
            "kwargs": {},
            "locks": []
        }
    }