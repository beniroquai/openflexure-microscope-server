from labthings.extensions import BaseExtension
from labthings import fields, find_component, find_extension
from openflexure_microscope.api.utilities.gui import build_gui
from labthings.views import View, ActionView, PropertyView
from time import sleep
from PIL import Image
import numpy as np
from picamerax.array import PiRGBArray

class PhaseContrastImageAction(ActionView):
    args = {
        "axis": fields.String(required=True),
        "color_red": fields.Integer(missing=0, allow_none=True),
        "color_green": fields.Integer(missing=0, allow_none=True),
        "color_blue": fields.Integer(missing=0, allow_none=True),
        "color_mode": fields.String(required=True),
        "options":fields.List(fields.String, missing=[], allow_none=True),
        "imaging_mode": fields.String(required=True)
    }

    def post(self, args):
        self.axis = args.get("axis")
        self.color = np.array([args.get("color_red") or 0, args.get("color_green") or 0, args.get("color_blue") or 0])
        self.color_mode = args.get("color_mode")
        self.options = args.get("options") or {}
        self.imaging_mode = args.get("imaging_mode")
        self.grid_illumination_extension = find_extension("org.openflexure.grid-illumination")
        self.microscope = find_component("org.openflexure.microscope")

        if "Save separate images" in self.options:
            self.save_partial = True
        else:
            self.save_partial = False

        self.old_led_matrix = self.grid_illumination_extension.led_grid.state_matrix
        
        if self.imaging_mode == "2D":
            self.phase_contrast_2d()
        elif self.imaging_mode == "3D":
            self.phase_contrast_3d()
        elif self.imaging_mode == "Fourier ptychography":
            self.fourier_ptychography()

    def fourier_ptychography(self):

        width = self.grid_illumination_extension.led_grid.width
        height = self.grid_illumination_extension.led_grid.height

        with self.microscope.camera.lock, self.microscope.stage.lock:
            for ii in range(width):
                for jj in range(height):
                    self.grid_illumination_extension.led_grid.set_shape('all',np.array([0,0,0]),True)
                    self.grid_illumination_extension.led_grid.set_pixel(ii,jj,self.color)
                    sleep(0.3)
                    image = self.capture(self.microscope.camera).astype(np.float16)
                    if self.color_mode == "greyscale":
                        image=np.average(image, axis=2)
                    if self.save_partial:
                        self.save_image(image, self.color_mode)
                    sleep(0.4)
            
            self.grid_illumination_extension.led_grid.set_state_matrix(self.old_led_matrix)       

    def phase_contrast_3d(self):

        width = self.grid_illumination_extension.led_grid.width
        height = self.grid_illumination_extension.led_grid.height

        if self.axis == "x":
            direction_length = width
            middle_position = np.floor(height/2).astype('int')
        else:
            direction_length = height
            middle_position = np.floor(width/2).astype('int')


        with self.microscope.camera.lock, self.microscope.stage.lock:
            for ii in range(direction_length):
                self.grid_illumination_extension.led_grid.set_shape('all',np.array([0,0,0]),True)
                if self.axis =="x":
                    self.grid_illumination_extension.led_grid.set_pixel(ii,middle_position,self.color)
                else:
                    self.grid_illumination_extension.led_grid.set_pixel(middle_position,ii,self.color)
                sleep(0.3)
                image = self.capture(self.microscope.camera).astype(np.float16)
                if self.color_mode == "greyscale":
                    image=np.average(image, axis=2)
                if self.save_partial:
                    self.save_image(image, self.color_mode)
                sleep(0.4)
            
            self.grid_illumination_extension.led_grid.set_state_matrix(self.old_led_matrix)

    def phase_contrast_2d(self):

        if self.axis == "x":
            shapes = ["left", "right", "all"]
        else:
            shapes = ["top", "bottom","all"]

        with self.microscope.camera.lock, self.microscope.stage.lock:
            self.grid_illumination_extension.led_grid.set_shape(shapes[0], self.color, True)
            sleep(0.3)
            image1 = self.capture(self.microscope.camera).astype(np.float16)
            self.grid_illumination_extension.led_grid.set_shape(shapes[1], self.color, True)
            sleep(0.4)
            image2 = self.capture(self.microscope.camera).astype(np.float16)
            self.grid_illumination_extension.led_grid.set_shape(shapes[0], self.color, False)
            sleep(0.4)
            brightfield = self.capture(self.microscope.camera).astype(np.float16)

            #Turn off during processing
            self.grid_illumination_extension.led_grid.set_shape(shapes[0],np.array([0,0,0]),True)
            #some hacky processing needed to reduce memory usage.
            #At full resolution saving and converting to grayscale at the end results in OOM kill
                     
            if self.color_mode == "greyscale":
                image1 = np.average(image1, axis=2)
                image2 = np.average(image2, axis=2)
                brightfield = np.average(brightfield, axis=2)

            if self.save_partial:
                self.save_image(image1, self.color_mode)
                self.save_image(image2, self.color_mode)
                self.save_image(brightfield, self.color_mode)
            self.grid_illumination_extension.led_grid.set_state_matrix(self.old_led_matrix)

            image2 -= image1
            image1 = None
            brightfield += 1e-12*np.ones(brightfield.shape, dtype=np.float16)
            image2 *= 255.0
            image = np.absolute(image2/brightfield)
            image2 = None
            brightfield = None
            self.save_image(image, self.color_mode)

    
    def capture(self, camera):
        with PiRGBArray(camera.camera, size=tuple(camera.camera.MAX_RESOLUTION)) as output:
            camera.stop_stream_recording()
            camera.camera.resolution = tuple(camera.camera.MAX_RESOLUTION)
            camera.camera.capture(output, format='rgb')
            camera.start_stream_recording()
            return output.array

    def save_image(self, image_data, color_mode = "rgb"):
        capture_object = self.microscope.captures.new_image(False, None, "PhaseContrast")
        image = Image.fromarray(image_data.astype(np.uint8), "RGB" if color_mode == "rgb" else "L")
        image.save(capture_object.stream, format="jpeg")
        capture_object.flush()
        capture_object.put_and_save( #only needed for 2.7, done in flush() in 2.8
            metadata={
                "image": {
                    "id": capture_object.id,
                    "name": capture_object.name,
                    "acquisitionDate": capture_object.datetime.isoformat(),
                    "format": capture_object.format,
                    "tags": capture_object.tags,
                    "annotations": capture_object.annotations,
                }
            })

# Create the extension class
class PhaseContrastExtension(BaseExtension):
    def __init__(self):
        super().__init__("org.openflexure.phase-contrast", version="0.0.1")

        def gui_func():
            return {
                "icon": "border_clear",
                "forms": [
                    {
                        "name": "Capture a phase contrast image",
                        "route": "/capture-phase-contrast",
                        "isTask": True,
                        "isCollapsible": True,
                        "submitLabel": "Capture",
                        "schema": [
                            {
                                "fieldType": "selectList",
                                "name": "imaging_mode",
                                "label": "Imaging mode",
                                "value": "2D",
                                "options": ["2D", "3D", "Fourier ptychography"]
                            },
                            {
                                "fieldType": "selectList",
                                "name": "axis",
                                "label": "Axis",
                                "value": "x",
                                "options": ["x", "y"]
                            },
                            {
                                "fieldType": "selectList",
                                "name": "color_mode",
                                "label": "Capture colour mode",
                                "value": "greyscale",
                                "options": ["greyscale", "rgb"]
                            },
                            [{
                                "fieldType": "numberInput",
                                "name": "color_red",
                                "label": "Red",
                                "min": 0,
                                "default": 0,
                                "max": 255
                            },
                            {
                                "fieldType": "numberInput",
                                "name": "color_green",
                                "label": "Green",
                                "min": 0,
                                "default": 0,
                                "max": 255
                            },
                            {
                                "fieldType": "numberInput",
                                "name": "color_blue",
                                "label": "Blue",
                                "min": 0,
                                "default": 0,
                                "max": 255
                            }],
                            {
                                "fieldType": "checkList",
                                "name": "options",
                                "label": "Options",
                                "value": [],
                                "options": ["Save separate images"]
                            },
                        ]
                    }
                ]}

        self.add_view(PhaseContrastImageAction, "/capture-phase-contrast")
        self.add_meta("gui", build_gui(gui_func, self))