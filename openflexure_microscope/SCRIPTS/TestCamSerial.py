#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import cv2
from laser import laser
from lens import lens
import serial
import time
from IPython.display import clear_output
import matplotlib
matplotlib.interactive(False)

# gstreamer_pipeline returns a GStreamer pipeline for capturing from the CSI camera
# Defaults to 1280x720 @ 60fps
# Flip the image by setting the flip_method (most common values: 0 and 2)
# display_width and display_height determine the size of the window on the screen


def gstreamer_pipeline(
    capture_width=1280,
    capture_height=720,
    display_width=1280,
    display_height=720,
    framerate=60,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor_id=0 ! "
        "video/x-raw(memory:NVMM), "
        "width=(int)%d, height=(int)%d, "
        "format=(string)NV12, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )


mycam_cmd = gstreamer_pipeline(framerate=21)
cap = cv2.VideoCapture(mycam_cmd) 

# let the camera warm up
for i in range(10):
    state,img = cap.read()
    #
    print(np.mean(img))
plt.figure(i), plt.imshow(img),plt.colorbar(), plt.show
print(img.shape)
type(img)


# open the lens and move it 
serialport = "/dev/ttyUSB0"
if not('serialconnection' in locals() and serialconnection.is_open):
        serialconnection = serial.Serial(serialport,115200,timeout=1) # Open grbl serial port

print('Initializing Lens 1')
# init lens
lens_1 = lens(serialconnection, lens_id = 1)
pos_x2 = lens_1.get_position(direction="X")
lens_1.move(pos_x2+1000, "X")


def showstream(i, frame1, frame2):

    plt.subplot(121), plt.imshow(frame1),plt.colorbar()
    plt.subplot(121), plt.imshow(frame2),plt.colorbar()
    plt.title("Frame: "+str(i)), plt.show(block=False)

    
    clear_output(wait=True)


try:
    mydirection = "X"
    lens_1.move(10000, "Z")
    for i in range(100):
        
        lens_1.move(10000, mydirection)
        _,img1 = cap.read()  
        print(type(img))
        print(img.shape)
        time.sleep(.2)
        
        lens_1.move(0, mydirection)
        _,img2 = cap.read()
        showstream(i, img1, img2)
        time.sleep(.2)


    lens_1.move(0, "Z")
    lens_1.move(0, "X")
finally:
    serialconnection.close()
    cap.release()

