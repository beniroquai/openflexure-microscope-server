import numpy as np
import matplotlib.pyplot as plt
import cv2
from laser import laser
from lens import lens
import serial
import time
from IPython.display import clear_output
import matplotlib
import tifffile as tif


# gstreamer_pipeline returns a GStreamer pipeline for capturing from the CSI camera
# Defaults to 1280x720 @ 60fps
# Flip the image by setting the flip_method (most common values: 0 and 2)
# display_width and display_height determine the size of the window on the screen


def gstreamer_pipeline(
    capture_width=1280,
    capture_height=720,
    display_width=1280,
    display_height=720,
    framerate=60,
    flip_method=0,
):
    return (
        "nvarguscamerasrc ! "
        "video/x-raw(memory:NVMM), "
        "width=(int)%d, height=(int)%d, "
        "format=(string)NV12, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )

# open the lens and move it 
serialport = "/dev/ttyUSB1"
if not('serialconnection' in locals() and serialconnection.is_open):
        serialconnection = serial.Serial(serialport,115200,timeout=1) # Open grbl serial port

print('Initializing Lens 1')
# init lens
lens_1 = lens(serialconnection, lens_id = 1)


# To flip the image, modify the flip_method parameter (0 and 2 are the most common)
print(gstreamer_pipeline(flip_method=0))
cap = cv2.VideoCapture(gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)

# let the camera warm up
for _ in range(20):
    ret_val, img = cap.read()
    print(np.mean(img))
            
rangex = np.int16(np.array(range(7100,7200,75)))
rangez = np.int16(np.array(range(300,360,1)))
if cap.isOpened():
    window_handle = cv2.namedWindow("CSI Camera", cv2.WINDOW_AUTOSIZE)
    # Window
    for ix in rangex:#range(100):
        lens_1.move(ix, "X")
        time.sleep(.1)
        for iz in rangez: #range(100): #while cv2.getWindowProperty("CSI Camera", 0) >= 0:
            lens_1.move(iz, "Z")
            time.sleep(.05)
            if iz == np.min(rangez):
                is_append = False
                time.sleep(.1) # let it settle
            else:
                is_append = True
            #grab image and save it
            ret_val, img = cap.read()
            img = np.uint8(np.mean(img, -1))

            #cv2.imwrite("/home/bene/OFM/openflexure-microscope-server/openflexure_microscope/SCRIPTS/myscan_ix_"+str(ix)+"_iz_"+str(iz)+".tif", img)
            tif.imsave("/home/bene/OFM/openflexure-microscope-server/openflexure_microscope/SCRIPTS/scanstack.tif", img, append=is_append)
            #cv2.imshow("CSI Camera", img)
            # This also acts as
            keyCode = cv2.waitKey(30) & 0xFF
            # Stop the program on the ESC key
            #time.sleep(.5)

            if keyCode == 27:
                break
    cap.release()
    cv2.destroyAllWindows()
else:
    print("Unable to open camera")

lens_1.move(0, "X")
lens_1.move(0, "Z")


serialconnection.close()
cap.release()
