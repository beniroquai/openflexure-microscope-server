posx_mm = 60
posy_mm = 70
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 19:20:37 2021

@author: bene
"""
import matplotlib.pyplot as plt
import numpy as np
import openflexure_microscope_client as ofm_client
microscope = ofm_client.MicroscopeClient("21.3.2.3", port=5000)

def setpos(x,y,z):
    return {'x': x, 'y': y, 'z': z}

pos = microscope.position
starting_pos = pos.copy()


print('Moving to Zero coordinate')
pos = setpos(0,0,0)
microscope.move(pos)
input("Hit enter when you have noted the position")

print('Moving to X')
pos = setpos(10000,0,0)
microscope.move(pos)
myx = input("What is the distance I moved in X?")
microscope.move(setpos(0,0,0))

print('Moving to Y')
pos = setpos(0,10000,0)
microscope.move(pos)
myy = input("What is the distance I moved in Y?")


print('Moving to Zero coordinate')
pos = setpos(0,0,0)
microscope.move(pos)

print('Moving to Forth')
pos = setpos(10000,10000,0)
microscope.move(pos)

print('Moving to Back')
pos = setpos(0,0,0)
microscope.move(pos)

# Check the microscope will autofocus
#ret = microscope.autofocus()

#%% Acquire an image for sanity-checking too
zsteps = pos['z'] - 100*np.arange(-5,6)
for i in range(10):
    pos['z'] = zsteps[i]
    print(pos)
    microscope.move(pos)
    image = microscope.grab_image()
    myarray = np.array(image).copy()
    plt.title('Z-pos: '+str(zsteps[i]))
    plt.imshow(myarray[:,:,1]), plt.show()

#%%
print("Active microscope extensions")
for k in microscope.extensions.keys():
    print(k)
