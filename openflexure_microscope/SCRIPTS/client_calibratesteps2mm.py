#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 19:20:37 2021

@author: bene
"""
import matplotlib.pyplot as plt
import numpy as np
import openflexure_microscope_client as ofm_client
microscope = ofm_client.MicroscopeClient("21.3.2.3", port=5000)

def setpos(x,y=None,z=None):
    if type(x)==tuple or type(x)==np.ndarray:
        x,y,z=x[0],x[1],x[2]
    
    return {'x': x, 'y': y, 'z': z}

def getpos(pos):
    return np.array(((pos['x']),(pos['y']),(pos['z'])))

if(0):

    pos = microscope.position
    starting_pos = pos.copy()
    
    stepsx=10000
    stepsy=10000
    
    print('Moving to Zero coordinate')
    pos = setpos(0,0,0)
    microscope.move(pos)
    myX1 = input("What is the X coordinate`? (in mm with '.'")
    myY1 = input("What is the Y coordinate`?")
    input("Hit enter when you have noted the position")
    
    print('Moving to X')
    pos = setpos(stepsx,0,0)
    microscope.move(pos)
    myX2 = input("What is the distance I moved in X?")
    microscope.move(setpos(0,0,0))
    
    print('Moving to Y')
    pos = setpos(0,stepsy,0)
    microscope.move(pos)
    myY2 = input("What is the distance I moved in Y?")
    
    
    # calculate mm per step
    stepunitx = abs(float(myX1)-float(myX2))/stepsx # in mm
    stepunity = abs(float(myY1)-float(myY2))/stepsy # in mm
    
    np.save('calibxy.npy', (stepunitx,stepunity))
    print("One step means: "+(str(stepunitx*1e3))+" mum in X, and "+(str(stepunity*1e3))+" um in Y")
    # One step means: 1.5910000000000004 mum in X, and 1.6179999999999999 um in Y

else:
    (stepunitx,stepunity)= np.load('calibxy.npy')

#%% move to the edges 
# 60 mm in x
# 70 mm in y from the zero
posx_mm = 40
posy_mm = 40
myposarray = np.array(((0,0,0), (posx_mm,0,0),(posx_mm,posy_mm,0),(0,posy_mm,0)))


# move to the zero
microscope.set_zero()
zeropos = microscope.position
print(zeropos)

def go_to(pos_x,pos_y=None,pos_z=None):
    """
    Move the stage to physical coordinates

    Parameters
    ----------√
    pos_x : TYPE
        DESCRIPTION.
    pos_y : TYPE, optional
        DESCRIPTION. The default is None.
    pos_z : TYPE, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    None.

    """
    if type(pos_x)==np.ndarray:
        pos_x,pos_y,pos_z=pos_x[0],pos_x[1],pos_x[2]
    mypos = np.array((int(pos_x/stepunitx),int(pos_y/stepunity),int(pos_z/stepunity)))
    stepstorun = setpos(mypos-getpos(zeropos))
    microscope.move(stepstorun)
    pos = microscope.position
    return pos

#%%
for i in range(myposarray.shape[0]):
    print("Move to: " +str(myposarray[i,:])+"mm")
    go_to(myposarray[i,:])    

    input('Hit Enter if in focus')    
    image = microscope.grab_image()
    myarray = np.array(image).copy()
    plt.title('Z-pos: '+str(myposarray[i,:])+"mm")
    plt.imshow(myarray[:,:,1]), plt.show()


